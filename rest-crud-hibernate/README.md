# REST CRUD

Tutorials related to this project:

1. [Spring boot crud operations example with hibernate](https://howtodoinjava.com/spring-boot2/spring-boot-crud-hibernate/)


## To import this project into Eclipse IDE

If you want to import these project into your local eclipse setup - 

1. Download the project as zip file into your computer
2. Unzip the project at your desired location
3. Import the project into Eclipse as existing maven project

```
File > Import... > Maven > Existing Maven Projects
```

4. Done